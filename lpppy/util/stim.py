#!/usr/bin/env python
# -*- coding: utf-8 -*-
from numpy import copyto, array, arange, meshgrid, sin, cos, exp, pi, int32

from pygame.surface import Surface
from pygame.surfarray import pixels3d

from lpppy.model.graphic import Graphic


def gabor_patch(size, sigma, theta, lambda_, phase, **kw):
    """Create a gray Gabor patch, centered on (127, 127, 127) in color-space.
    Arguments that accept tuples will assume an equivalent second value if an
    int or float is passed.

    size : int or 2-tuple
        X, Y dimensions in pixels

    sigma : int or 2-tuple
        X, Y standard deviation of Gaussian mask

    theta : int or 2-tuple
        Orientation, in radians, of Gaussian mask and grating, respectively

    lambda : int
        Spatial frequency in pixels

    phase : float
        Phase of spatial frequency for grating

    offset : int or 2-tuple
        X, Y offset of Gaussian mask within the bounding box
        [Default: (0, 0)]

    contrast : float
        Contrast of Gabor patch, from 0.0 to 1.0
        [Default: 1.0]

    trim : float
        Cutoff value for Gaussian values.
        [Default: 0.]
    """
    con = kw.get('contrast', 1.0)

    # Unpack arguments as needed
    if isinstance(size, int):
        size = (size, size)
    size = array(size, dtype=float)

    st = []
    for var in (sigma, theta, kw.get('offset', 0)):
        if isinstance(var, int) or isinstance(var, float):
            var = map(float, (var, var))
        st.append(var)
    (sigma1, sigma2), (theta1, theta2), (xoff, yoff) = st

    # Define bounding box & coordinates
    x, y = meshgrid(*map(arange, -size / 2, size / 2))

    x -= xoff
    y -= yoff

    # rotate coordinates
    xt = x * (cos(pi - theta1)) + y * (sin(pi - theta1))
    yt = y * (cos(pi - theta1)) - x * (sin(pi - theta1))
    xt2 = x * (cos(pi / 2 - theta2)) + y * (sin(pi / 2 - theta2))

    # make Gabor patch
    # use 0.5*contrast to set max and min values around zero
    grating = (0.5 * con) * cos(xt2 * ((2.0 * pi) / lambda_) + phase)
    gauss = exp(  # generates values between 0-1;
        -(xt * xt) / (2 * sigma1 * sigma1) - (yt * yt) / (2 * sigma2 * sigma2)
    )
    gauss[gauss < kw.get('trim', 0)] = 0
    return (grating * gauss)


class Gabor(Graphic):
    """Graphic class for Gabor patches.  See `gabor_patch`."""
    def __init__(self, event_manager, **kw):
        self._size = sz = kw.pop('size', None)
        self._sigma = sg = kw.pop('sigma', None)
        self._theta = th = kw.pop('theta', None)
        self._lmbda = l = kw.pop('lmbda', None)
        self._phase = p = kw.pop('phase', None)
        self._offset = o = kw.pop('offset', 0)
        self._trim = t = kw.pop('trim', .005)
        self._contrast = c = kw.pop('contrast', 1.0)
        gabor = self._gabor(sz, sg, th, l, p, offset=o, trim=t, contrast=c)
        super(Gabor, self).__init__(event_manager, gabor, **kw)

    def _gabor(self, *args, **kw):
        surf = Surface(self.size)
        Graphic.assert_referenceable_surf(surf)

        gabor = (((gabor_patch(*args, **kw) + 1) / 2) * 255).astype(int32)
        gabor = array((gabor, gabor, gabor)).T
        copyto(pixels3d(surf), gabor, casting='unsafe')
        return surf

    def _update(self, value, field):
        if value != getattr(self, field):
            setattr(self, field, value)
            self._surf = self._gabor(self._size,
                                     self._sigma,
                                     self._theta,
                                     self._lmbda,
                                     self._phase,
                                     contrast=self._contrast,
                                     offset=self._offset,
                                     trim=self._trim)

    @property
    def size(self):
        return self._size

    @size.setter
    def size(self, value):
        if isinstance(value, int) or isinstance(value, float):
            value = (value, value)
        self._update(value, '_size')

    @property
    def sigma(self):
        return self._sigma

    @sigma.setter
    def sigma(self, value):
        if isinstance(value, int) or isinstance(value, float):
            value = (value, value)
        self._update(value, '_sigma')

    @property
    def theta(self):
        return self._theta

    @theta.setter
    def theta(self, value):
        if isinstance(value, int) or isinstance(value, float):
            value = (value, value)
        self._update(value, '_theta')

    @property
    def lmbda(self):
        return self._lmbda

    @lmbda.setter
    def lmbda(self, value):
        self._update(value, '_lmbda')

    @property
    def phase(self):
        return self._phase

    @phase.setter
    def phase(self, value):
        self._update(value, '_phase')

    @property
    def offset(self):
        return self._offset

    @offset.setter
    def offset(self, value):
        if isinstance(value, int) or isinstance(value, float):
            value = (value, value)
        self._update(value, '_offset')

    @property
    def trim(self):
        return self._trim

    @trim.setter
    def trim(self, value):
        self._update(value, '_trim')

    @property
    def contrast(self):
        return self._contrast

    @contrast.setter
    def contrast(self, value):
        self._update(value, '_contrast')
