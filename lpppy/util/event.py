#!/usr/bin/env python
# -*- coding: utf-8 -*-
from collections import namedtuple


class EventDispatcher(object):
    """Conveniece class for posting custom events to the EventManager.  Useful
    for custom signals that need to be handled by multiple listeners.

    event_manager : lpppy.core.EventManager instance

    type_str : str
        Type name given to the event being posted.

    *fields : iterable of strings
        Field names to be assigned to the event type.
    """

    def __init__(self, event_manager, type_str, *fields):
        self.event_manager = event_manager
        self._ev = namedtuple('EventDispatcher::{0}'.format(type_str), fields)
        self._nargs = len(fields)

    def __call__(self, *args):
        if len(args) != self._nargs:
            TypeError(
                '{0} takes exactly {1} arguments ({2} given)'
            ).format(self.__class__.__name__, self._nargs, len(args))

        self.event_manager.post(self._ev(*args))
