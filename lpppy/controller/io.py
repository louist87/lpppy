#!/usr/bin/env python
# -*- coding: utf-8 -*-
from collections import namedtuple
from itertools import imap, ifilter

from serial import Serial

from pygame.locals import *
from pygame.event import get as get_pg_events

from lpppy.core import Listener, QUIT_EVENT

KEYEVS = set((KEYUP, KEYDOWN))
MOUSEEVS = set((MOUSEBUTTONDOWN, MOUSEBUTTONUP, MOUSEMOTION))
JOYEVS = set((JOYAXISMOTION, JOYBALLMOTION, JOYBUTTONUP, JOYBUTTONDOWN,
              JOYHATMOTION))

IOEVS = KEYEVS.union(MOUSEEVS).union(JOYEVS)


class BasicIOController(Listener):
    def _process_event(self, event):
        if event.type not in IOEVS:  # Filter non-IO events
            return event if event.type == QUIT else None

        # Exit on ctrl + c
        if (getattr(event, 'mod', 0) & KMOD_CTRL) and (event.key == K_c):
            return QUIT_EVENT

        return event

    def on_frame(self, _):
        self._read_pg_events()

    def on_tick(self, _):
        self._read_pg_events()

    def hook(self, event):
        """Hook function for implementing custom logic in BasicIOController
        subclasses.

        By default, returns an unchanged event.
        """
        return event

    def _read_pg_events(self):
        for ev in ifilter(None, imap(self._process_event, get_pg_events())):
            self.event_manager.post(self.hook(ev))


class SerialController(Listener):
    _Event = namedtuple('SerialEvent', ('type', 'bytes'))

    def __init__(self, event_manager, port=0, baud_rate=9600):
        super(SerialController, self).__init__(event_manager)

        self._serial = Serial(port=port, baudrate=baud_rate, timeout=0)

    def on_tick(self, _):
        self._read_serial_events()

    def on_frame(self, _):
        self._read_serial_events()

    def _read_serial_events(self):
        char = self._serial.read()
        if char:
            self._post_event(char)

    def _post_event(self, char):
            self.event_manager.post(self._Event('serialportdata', char))


class FORPController(SerialController):
    _Event = namedtuple('fORPEvent', ('type', 'key', 'mod', 'unicode'))
    _scan_ttl_event = namedtuple('ScanTTLEvent', ('type',))('scanttl')
    code_table = {'1': u'b', '2': u'y', '3': u'g', '4': u'r', '5': 'scan'}

    def __init__(self, event_manager, port=0, baud_rate=19200):
        super(FORPController, self).__init__(
            event_manager, port=port, baud_rate=baud_rate
        )

    def _post_event(self, char):
        if char in '1234':
            self.event_manager.post(
                self._Event(2, ord(char), 0, self.code_table[char])
            )
        elif char == '5':
            self.event_manager.post(self._scan_ttl_event)
