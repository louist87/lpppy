#!/usr/bin/env python
# -*- coding: utf-8 -*-
from collections import namedtuple

import pygame as pg

from lpppy.core import Listener


class ClockController(Listener):
    """Generates `TickEvent` instances, serving as central engine and conveying
    temporal information.

    `TickEvent` instances do __not__ trigger a display flip, therefore
    `ClockController` should be used when you want to manually flip the display
    """
    _Event = namedtuple('TickEvent', ('type', 'elapsed', 'delta'))
    _evname = 'tick'

    def __init__(self, event_manager, frame_rate=0):
        super(ClockController, self).__init__(event_manager)

        self._clock = pg.time.Clock()
        self._abstime = 0
        self.frame_rate = frame_rate

        self.running = True

    def on_start(self, _):
        while self.running:
            dt = self._clock.tick_busy_loop(self.frame_rate)
            self._abstime += dt
            self.event_manager.post(
                self._Event(self._evname, self._abstime, dt)
            )

    def on_quit(self, _):
        self.running = False

    def on_t0(self, _):
        """Reset clock to zero, indicating a reference time for the experiment.
        """
        self._clock.tick_busy_loop()
        self._abstime = 0
