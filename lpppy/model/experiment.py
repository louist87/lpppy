#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""lpppy.model.experiment : base model classes for creating experiments."""
from collections import namedtuple
from types import GeneratorType
from weakref import WeakSet

from pygame import mixer

from lpppy.core import Listener
from lpppy.model.graphic import Background
from lpppy.model.audio import Sound


class AudioModel(object):
    _Play = namedtuple('AudioPlayEvent',
                       ('type', 'sound', 'loops', 'maxtime', 'fade', 'vol'))
    _Stop = namedtuple('AudioStopEvent', ('type', 'sound'))
    _Pause = namedtuple('AudioPauseUnpauseEvent', ('type'))
    _Fade = namedtuple('AudioFadeEvent', ('type', 'sound', 't'))

    def __init__(self, event_manager):
        self.event_manager = event_manager
        self._abuffer = {}  # key : pg.mixer.Sound

    def __iter__(self):
        return self._abuffer.iterkeys()

    def pop(self, key):
        self._abuffer.pop(key)

    def clear(self):
        self._abuffer.clear()

    def keys(self):
        return self._abuffer.keys()

    def setdefault(self, *args):
        return self._abuffer.setdefault(*args)

    def register_sound(self, key, *args, **kwargs):
        """Register a sound.

        key : hashable
            Key value to identify the Sound object.

        See lpppy.model.audio.Sound for additional arguments
        """
        self._abuffer[key] = Sound(*args, **kwargs)

    @property
    def num_channels(self):
        return mixer.get_num_channels()

    @num_channels.setter
    def num_channels(self, n):
        mixer.set_num_channels(n)

    @property
    def busy(self):
        return mixer.get_busy()

    def reserve_channel(self, n):
        """{0}""".format(mixer.set_reserved.__doc__)
        mixer.set_reserved(n)

    def find_channel(self, force=False):
        """{0}""".format(mixer.find_channel.__doc__)
        mixer.find_channel(force)

    def play(self, key, loops=0, maxtime=0, fade_ms=0, vol=None):
        self.event_manager.post(
            self._Play(
                'audio_play', self._abuffer[key], loops, maxtime, fade_ms, vol
            )
        )

    def stop(self, key=None):
        self.event_manager.post(self._Stop('audio_stop', self._abuffer[key]))

    def pause(self):
        self.event_manager.post(self._Pause('audio_pause'))

    def unpause(self):
        self.event_manager.post(self._Pause('audio_unpause'))

    def fadeout(self, key=None, t=1000):
        if key is not None:
            key = self._abuffer[key]
        self.event_manager.post(self._Pause('audio_fadeout', key, t))


class VideoModel(object):
    _flipevent = namedtuple('FlipEvent', ('type'))('flip')

    def __init__(self, event_manager, background=None):
        self.event_manager = event_manager

        self._graphics = {}  # zorder : set()
        self._backgroud = background or Background(event_manager)

    def _is_graphic_registered(self, graphic):
        if not self._graphics:
            return False

        sall = set()
        for s in self._graphics.itervalues():
            sall.update(s)
        return graphic in sall

    def register_graphic(self, graphic, zorder=0):
        """Register a graphic.

        graphic : instance
            Graphic instance to register

        zorder : int
            Z-order of the Graphic being added
        """
        assert not self._is_graphic_registered(graphic), \
            'Graphic {0} already registered'.format(graphic)

        if zorder < 0:
            zorder = 0
        self._graphics.setdefault(zorder, WeakSet()).add(graphic)

    def unregister_graphic(self, graphic, zorder=None):
        """Unregister a graphic.

        graphic : instance
            Graphic to unregister
        """
        if zorder is None:
            for z, layer in self._graphics.iteritems():
                layer.discard(graphic)
        else:
            self._graphics[zorder].remove(graphic)

    def clear(self):
        """Remove all graphics (excluding background)"""
        self._graphics.clear()

    def _iter_render_order(self):
        yield self._backgroud
        for layer in sorted(self._graphics):
            for g in self._graphics[layer]:
                yield g

    def display_all(self):
        for graphic in self._iter_render_order():
            graphic.draw()
        self.flip()

    def flip(self):
        self.event_manager.post(self._flipevent)


class Factory(object):
    """Factory class to manage the creation and iteration over blocks and/or
    trials.

    `Factory` is used by `Subject` and `Block` to produce `Block` and `Trial`
    instances, respectively.  `Factory.produce` can be overridden to implement
    custom instantiation logic (see docstring for `Factory.produce`).

    The following attributes are always assigned to each value produced:
        - parent : a reference to the parent `Segment` instance
        - audio_model : a reference to the parent's audio model
        - video model : a reference to the parent's video model
        - _num : an integer, which should not be modified

    parent : Segment
        The parent `Segment` instance.  This is typically a `Subject` instance
        or `Block` instance.
    """
    def __init__(self, parent):
        self.parent = parent
        self._segments = None

    def _iter_coroutines(self):
        for segment in self._produce(self.parent):
            for coroutine in segment:
                yield coroutine

    def produce(self, parent):
        """Produce should be overridden to implement the production logic
        behind the factory.

        NOTE:  references to the parent, its audio/video models and the
               block/trial number will be added automatically.

        parent : `Segment` instance
            Reference to the parent segment (typically a `Subject` or `Block`).

        return : iterable of `Segment` instances
        """
        raise NotImplementedError

    def iter_blocks(self):
        """Called when iterating over the object returned by `Factory.produce`,
        `Factory.iter_blocks` can be overridden to implement custom segment
        numbering.  This is useful for resuming interrupted experiments from
        a given segment (e.g. block).

        It is highly recommended that segment randomization be performed in
        `Factory.produce`, as this function is experimental and may be
        depricated.

        `Factory.iter_blocks` should produce tuples of (segment_number,
        segment_instance)
        """
        return enumerate(self._segments)

    def _produce(self, parent):
        self._segments = tuple(self.produce(parent))
        for i, segment in self.iter_blocks():
            setattr(segment, 'parent', self.parent)
            setattr(segment, 'video_model', self.parent.video_model)
            setattr(segment, 'audio_model', self.parent.audio_model)
            setattr(segment, '_num', i)
            yield segment


class Segment(Listener):
    def __init__(self, event_manager):
        # by default, a segment should not listen to avoid changing state
        # while it is not active.
        super(Segment, self).__init__(event_manager, listen=False)
        self._num = None
        self._coroutines = [self.pre, self.per, self.post]
        self._done = False

    def __repr__(self):
        return "<{0}> {1}".format(self.__class__.__name__, self._num)

    def __iter__(self):
        """Iterate through coroutines."""
        while not self._done:
            yield self._call_coroutine

    def _call_coroutine(self, t, dt):
        self.listening = True
        if not self._coroutines:
            return self.done(t, dt)

        # If coroutine is already initialized, execute it.
        if isinstance(self._coroutines[0], GeneratorType):
            try:
                return self._coroutines[0].send((t, dt))
            except StopIteration:
                self._coroutines.pop(0)
                return self._call_coroutine(t, dt)

        # Else, initialize the coroutine and run it for the first time.
        try:
            # 1.  Initialize the coroutine, passing arguments for first run.
            coroutine = self._coroutines.pop(0)(t, dt)
            if coroutine is None:
                # Coroutine will return None if it contains no yield statement.
                return self._call_coroutine(t, dt)

            self._coroutines.insert(0, coroutine)
            # 2.  Execute the first run.
            return self._coroutines[0].next()
        except StopIteration:  # can happen if base method is not overridden
            # No need to pop, since we popped when initializing the coroutine
            return self._call_coroutine(t, dt)

    def pre(self, t, dt):
        raise StopIteration

    def per(self, t, dt):
        raise StopIteration

    def post(self, t, dt):
        raise StopIteration

    def done(self, t, dt):
        self.listening = False
        self._done = True


class _ParentSegment(Segment):
    """Convenience subclass of `Segment` for all derived classes whose `per`
    method calls the coroutine of a child segment (usually `Subject` and
   `Block`)

    Overrides `Segment.per` and sets a default value for the `factory`
    attribute.
    """
    def __init__(self, event_manager):
        super(_ParentSegment, self).__init__(event_manager)
        self.factory = None

    def per(self, t, dt):
        for coroutine in self.factory._iter_coroutines():
            coroutine(t, dt)
            t, dt = yield


class Subject(_ParentSegment):
    def __init__(self, event_manager, sub_num, **kw):
        super(Subject, self).__init__(event_manager)
        self.listening = True  # Segments do not listen by default

        self.event_manager = event_manager
        self._num = sub_num
        self.audio_model = kw.pop('audio', AudioModel(event_manager))
        self.video_model = kw.pop('video', VideoModel(event_manager))

    @property
    def sub_num(self):
        return self._num

    def on_frame(self, event):
        """Callback for a frame event.
        Draw background, call frame listener, blit all visible items and flip
        frame buffer.
        """
        if self.video_model is not None:
            for graphic in self.video_model._iter_render_order():
                graphic._frame_listener(*event[1::])

        self.frame_listener(*event[1::])  # ('frame', total, delta)
        self.video_model.flip()

    def frame_listener(self, t, dt):
        """Implements logic that is executed between frames.  Called exactly
        once per frame.

        t : int
            Total time (in ms) since beginning of experiment.

        dt : int
            Time since last frame.
        """
        self._call_coroutine(t, dt)

    def on_tick(self, event):
        """Callback for Tick event.  Execute the current segment coroutine."""
        self._call_coroutine(*event[1::])

    def done(self, t, dt):
        from pygame.event import Event
        self.event_manager.post(Event(12))  # Post a pg Quit event


class Block(_ParentSegment):
    def __repr__(self):
        return "<{0}> {1}".format(self.__class__.__name__, self.block_num)

    @property
    def sub_num(self):
        return self.parent.sub_num

    @property
    def block_num(self):
        return self._num

    @property
    def subject(self):
        return self.parent


class Trial(Segment):
    def __repr__(self):
        return "<{0}> {1}".format(self.__class__.__name__, self.trial_num)

    @property
    def sub_num(self):
        return self.parent.sub_num

    @property
    def block_num(self):
        return self.parent.block_num

    @property
    def trial_num(self):
        return self._num

    @property
    def subject(self):
        return self.parent.parent

    @property
    def block(self):
        return self.parent
