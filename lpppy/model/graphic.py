#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""lpppy.model.graphic : Classes for sprites, backgrounds, fonts and all manner
of graphical elements that may be displayed.
"""
from collections import namedtuple
from contextlib import contextmanager

import pygame as pg
from pygame.color import THECOLORS

from lpppy.core import Listener


class Graphic(Listener):
    """A general `Listener` class for all graphical items including:
    - sprites
    - background
    - polygons, lines, etc...

    event_manager : lpppy.core.EventManager instance

    surface : pygame.surface.Surface instance

    dest : None or pg.surface.Surface
        Destination surface onto which the Graphic should be blitted.  If None,
        the Graphic is blitted to the main screen.
        [Default: None]

    position : 2-tuple
        (x, y) cartesian coordinates.
        [Default: (0, 0)]

    centered : bool
        If true, `position` kwarg is used to center the Graphic.
        [Default: True]

    visible : bool
        If true, the Graphic is blitted when Graphic.draw is called.
        [Default: True]

    register : None or VideoModel-like instance or 2-tuple
            If register is an instance with a `register_graphic` method, the
            `Graphic` instance will automatically be registered to said model.
            If register is a 2-tuple of (VideoModel, zorder), the zorder will
            be applied
            [Default: None]
    """
    _BlitEvent = namedtuple('BlitEvent',
                            ('type', 'position', 'destination', 'surface'))

    def __init__(self, event_manager, surface, register=None, **kw):
        super(Graphic, self).__init__(event_manager)

        self._surf = surface
        self.dest = kw.pop('dest', None)
        self.visible = kw.pop('visible', True)

        if not kw.pop('centered', True):
            self.x, self.y = kw.pop('position', (0, 0))
        else:
            self.center_on(kw.pop('position', (0, 0)))

        if register is not None:
            if isinstance(register, tuple):
                register, zo = register
            else:
                zo = 0
            register.register_graphic(self, zorder=zo)

    @property
    def position(self):
        return self.x, self.y

    @position.setter
    def position(self, xy):
        self.x, self.y = xy

    @property
    def size(self):
        return self._surf.get_size()

    @property
    def center(self):
        w, h = self._surf.get_size()
        return w / 2 + (w % 2), h / 2 + (h % 2)

    def center_on(self, xy):
        x, y = xy
        cx, cy = self.center
        self.position = (x - cx, y - cy)

    def draw(self):
        """Draw a graphic onto its parent Surface."""
        if self.visible:
            self.event_manager.post(
                self._BlitEvent('blit', self.position, self.dest, self._surf)
            )

    @contextmanager
    def show(self, vmodel):
        self.visible = True
        try:
            vmodel.display_all()
            yield self
        except:
            raise
        finally:
            self.visible = False
            vmodel.display_all()

    @staticmethod
    def assert_referenceable_surf(surf):
        """Check that a surface has a bit-depth that can be referenced
        by the `pygame.surfarray` submodule.

        Raises ValueError if surface bit-depth is not 24 or 32.
        """
        bs = surf.get_bitsize()
        if bs not in (32, 24):
            msg = ('Unreferenceable bit-depth {0}.  '
                   'Try forcing bit-depth to 24 or 32')
            raise ValueError(msg.format(bs))


class Background(Graphic):
    """Special Graphic which violates the Graphic API in a few subtle ways:
    1. Blitted before anything else
    2. No `x` or `y` attributes
    3. No `center` or `position` properties
    4. No `center_on` method

    event_manager : lpppy.core.EventManager instance

    color : 3-tuple or 4-tuple
        Background color
        (r, g, b) or (r, g, b, alpha)
        [Default: (127, 127, 127, 0)]

    graphic : lpppy.model.Graphic instance
        Graphic that sits on top of the base color.
        [Default : None]
    """
    _FillEvent = namedtuple('FillEvent', ('type', 'color', 'graphic'))

    def __init__(self, event_manager, color=THECOLORS['gray50'], graphic=None):
        super(Background, self).__init__(event_manager, None, centered=False)

        self.color = color
        self.graphic = graphic

        delattr(self, 'x')
        delattr(self, 'y')

    @property
    def center(self):
        raise NotImplementedError

    @property
    def position(self):
        raise NotImplementedError

    def center_on(self, xy):
        raise NotImplementedError

    def _frame_listener(self, total_t, delta_t):
        self.frame_listener(total_t, delta_t)  # handle user-defined logic
        self.draw()

    def frame_listener(self, total_t, delta_t):
        """Called once per frame.  Used to update the Graphic"""
        pass

    def draw(self):
        if self.visible:
            if self.graphic is not None:
                s = self.graphic._surf
            else:
                s = None

            self.event_manager.post(self._FillEvent('fill', self.color, s))


class TextFactory(object):
    def __init__(self, name=None, size=24, bold=False, italic=False):
        self._font = pg.font.Font(name, size, bold=bold, italic=italic)

    @property
    def font(self):
        return self._font

    @font.setter
    def font(self, *args, **kwargs):
        self._font = pg.font.Font(*args, **kwargs)

    def size(self, text):
        return self._font.size(text)

    def render(self, event_manager, text, **kw):
        """Create a `Graphic` instance with the specified text.

        event_manager : lpppy.core.EventManager instance

        text : str or unicode
            Text to display.  If unicode, must be Pygame compatible.

        bold : bool
            If True, render the text in bold font.
            [Default: False]

        underline : bool
            If True, render the text in an underlined font.
            [Default: False]

        antialias : bool
            If true, render the text with Pygame's antialiasing enabled.
            See pygame.font.Font.render
            [Default: True]

        color : 3-tuple or 4-tuple
            Text color as (R, G, B[, alpha])
            See pygame.font.Font.render
            [Default: (0, 0, 0)]

        background : 3-tuple, 4-tuple or None
            Background color.  See pygame.font.Font.render
            [Default: None]

        **kw : see Graphic
        """
        bold, ul = map(kw.pop, ('bold', 'underline'), (False, False))
        if bold:
            self._surf.set_bold(bold)
        if ul:
            self._surf.set_underline(ul)

        bg = kw.pop('background', None)
        if bg is None:
            surf = self._font.render(text,
                                     kw.pop('antialias', True),
                                     kw.pop('color', (0, 0, 0)))
        else:
            surf = self._font.render(text,
                                     kw.pop('antialias', True),
                                     kw.pop('color', (0, 0, 0)),
                                     kw.pop('background'))

        if bold:
            self._surf.set_bold(False)
        if ul:
            self._surf.set_underline(False)

        return Graphic(event_manager, surf, **kw)
