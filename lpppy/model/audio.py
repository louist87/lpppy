#!/usr/bin/env python
# -*- coding: utf-8 -*-
import contextlib

from pygame.mixer import Sound as PgSound


class Sound(object):
    """Create a new sound buffer object which can be played with through the
    lpppy.view.PygameAudioView.

    Sounds can be initialized by simply passing a file path (str), buffer,
    array, or arbitrary object as `Sound`'s first  parameter.  In such cases,
    `Sound` will attempt to infer the correct type and load the data into an
    audio buffer.

    To avoid ambiguity, a keyword can be used to specify the type of object
    being passed:

    file : str or file-like instance
        Path to a .wav or .ogg object or file-like instance.

    buffer : C-level array or PyBuffer interface

    array : Numpy or buffer array

    Additional parameters may be set through keword arguments:

    vol : float
        Volume, ranging from 0.0 to 1.0 [Default: 1.0]
    """
    def __init__(self, *args, **kwargs):
        vol = kwargs.pop('vol', 1.0)
        self._sound = PgSound(*args, **kwargs)
        self.volume = vol

    @property
    def volume(self):
        return self._sound.get_volume()

    @volume.setter
    def volume(self, volume):
        self._sound.set_volume(volume)

    @contextlib.contextmanager
    def set_volume(self, volume):
        pre = self.volume
        self.volume = volume
        try:
            yield self
        except:
            raise
        finally:
            self.volume = pre

    @property
    def length(self):
        return self._sound.get_length()

    @property
    def num_channels(self):
        return self._sound.get_num_channels()

    @property
    def raw(self):
        return self._sound.get_raw()

    def _play(self, loops=0, maxtime=0, fade_ms=0):
        """{0}""".format(self._sound.play.__doc__)
        return self._sound.play(loops, maxtime, fade_ms)

    def _stop(self):
        """{0}""".format(self._sound.stop.__doc__)
        self._sound.stop()

    def _fadeout(self, time):
        """{0}""".format(self._sound.fadeout.__doc__)
        self._sound.fadeout(time)
