#!/usr/bin/env python
# -*- coding: utf-8 -*-
import pygame as pg
from pygame.locals import *

from lpppy import __version__
from lpppy.core import Listener


class PygameDisplayView(Listener):
    """Pygame-based display listener.
    Handles all display operations (blit, fill, flip).  Like all View
    constructs `PygameDisplayView` will not modify any Model or Controller.

    event_manager : EventManager instance

    resolution : 2-tuple

    flags : int
        default: 1073741825 (pg.HWSURFACE | pg.DOUBLEBUF)

    depth : int
        default 0

    fullscreen : bool
        default False

    """
    def __init__(self, event_manager, resolution=(800, 600), **kw):
        super(PygameDisplayView, self).__init__(event_manager)

        depth = kw.get('depth', 0)

        # Set SDL flags
        kw['flags'] = int(kw.pop('dbuf', True)) * pg.DOUBLEBUF
        kw['flags'] |= int(kw.pop('hwsurf', True)) * pg.HWSURFACE
        kw['flags'] |= int(kw.pop('opengl', False)) * pg.OPENGL
        kw['flags'] |= int(kw.pop('fullscreen', False)) * pg.FULLSCREEN

        # Initialize display
        self.screen = pg.display.set_mode(resolution, kw['flags'], depth)
        pg.display.set_caption('LPPPY v{0}'.format(__version__))

        # Mouse cursor
        pg.mouse.set_visible(int(kw.get('mouse_visible', True)))

    def on_flip(self, _):
        pg.display.flip()

    def on_fill(self, event):
        self.screen.fill(event.color)
        if event.graphic is not None:
            self.screen.blit(event.graphic, (0, 0))

    def on_blit(self, event):
        if event.destination is None:
            ex, ey = event.position
            sx, sy = self.screen.get_size()
            self.screen.blit(event.surface, ((sx / 2) + ex, (sy / 2) + ey))
        else:
            event.destination.blit(event.surface, event.position)


class PygameAudioView(Listener):
    """Pygame-based audio listener.
    Handles all audio output operations (play, stop, fade, etc...) for both
    individual audio buffers and the audio mixer (lpppy.model.AudioModel)

    event_manager : lpppy.core.EventManager instance
    """
    def on_audio_play(self, event):
        if event.vol is not None:
            with event.vol.set_volume(event.vol) as sound:
                return sound.play(event.loops, event.maxtime, event.fade)
        event.sound._play(event.loops, event.maxtime, event.fade)

    def on_audio_stop(self, event):
        if event.sound is None:
            return pg.mixer.stop()
        event.sound._stop()

    def on_audio_pause(self, _):
        pg.mixer.pause()

    def on_audio_unpause(self, _):
        pg.mixer.unpause()

    def on_audio_fadeout(self, event):
        if event.sound is None:
            return pg.mixer.fadeout(event.t)
        event.sound._fadeout(event.t)
