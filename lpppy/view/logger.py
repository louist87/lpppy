#!/usr/bin/env python
# -*- coding: utf-8 -*-
from math import sqrt

from lpppy.core import Listener


class BaseLogger(Listener):
    """Logger base class.

    event_manager : lpppy.core.EventManager instance

    console : bool
        If true, output log elements to console
        [Default: True]

    file : str or None
        If None, do not log to file.  Else, save to the designated file.
        [Default: None]

    format : str
        Formatting string for handler(s).
        Default: "%(asctime)-15s - %(name)-12s - %(levelname)-8s: %(message)s"

    file_format : str or None
        If None, use the console format string to format file-logged messages.
        Otherwise, `file_format` should be a format string that will be applied
        to elements in the log file.
        [Default: None]
    """
    format = "%(asctime)-15s - %(name)-12s - %(levelname)-8s: %(message)s"

    def __init__(self, event_manager, name, **kw):
        super(BaseLogger, self).__init__(event_manager)

        import logging
        self.name = name
        self.logger = logging.getLogger(self.name)
        self.logger.setLevel(getattr(logging, kw.pop('level', 'INFO').upper()))

        # Set Handlers and Formatters
        format = kw.get('format', BaseLogger.format)

        if kw.pop('console', True):
            h = logging.StreamHandler()
            h.setFormatter(logging.Formatter(format))
            self.logger.addHandler(h)

        if kw.get('file') is not None:
            h = logging.FileHandler(kw['file'],
                                    mode=kw.pop('mode', 'a'),
                                    encoding=kw.pop('encoding', None),
                                    delay=kw.pop('delay', 0))
            h.setFormatter(logging.Formatter(kw.pop('file_format', format)))
            self.logger.addHandler(h)


class GreedyLogger(BaseLogger):
    """Logger that logs everything.

    exclude : list of strings
        Type of events to exclude.  By default, FrameEvents are not logged.
    """
    def __init__(self, event_manager, name, **kwargs):
        self.exclude = set(kwargs.pop('exclude', ('frame', 'tick')))
        super(GreedyLogger, self).__init__(event_manager, name, **kwargs)

    def notify(self, event):
        if event.type not in self.exclude:
            self.logger.info(event)


class FrameLogger(BaseLogger):
    _frame_msg = "Frame(elapsed={e.elapsed}, delta={e.delta})"

    def on_frame(self, event):
        self.logger.info(self._frame_msg.format(e=event))


class BasicInputLogger(BaseLogger):
    _msgs = {2: 'KeyDown(key={e.key}, mod={e.mod}, unicode={unicode})',
             3: 'KeyUp(key={e.key}, mod={e.mod})',
             4: 'MouseMotion(pos={e.pos}, rel={e.rel}, buttons={e.buttons})',
             5: 'MouseButtonDown(pos={e.pos}, button={e.button})',
             6: 'MouseButtonUp(pos={e.pos}, button={e.button})',
             7: 'JoyAxisMotion(joy={e.joy}, axis={e.axis}, value={e.value})',
             8: 'JoyBallMotion(joy={e.joy}, ball={e.ball}, rel={e.rel}))',
             9: 'JoyHatMotion(joy={e.joy}, hat={e.hat}, value={e.value})',
             10: 'JoyButtonDown(joy={e.joy}, button={e.button}))',
             11: 'JoyButtonUp(joy={e.joy}, button={e.button})'}

    _pgio = {'keyboard': set(xrange(2, 4)),
             'mouse': set(xrange(4, 7)),
             'joystick': set(xrange(7, 12))}

    _wspace = {'\t': '\\t', '\n': '\\n', '\x0b': '\\x0b', '\r': '\\r'}

    def __init__(self, *args, **kwargs):
        # Track only input from requested devices
        self._tracking = set()
        for k, v in BasicInputLogger._pgio.iteritems():
            if kwargs.pop(k, True):
                self._tracking.update(v)

        super(BasicInputLogger, self).__init__(*args, **kwargs)

    def log_io_event(self, event):
        u = getattr(event, 'unicode', '')
        self.logger.info(
            self._msgs[event.type].format(
                e=event, unicode=self._wspace.get(u, u)
            )
        )

    def notify(self, event):
        if event.type in self._tracking:
            # the event is a PyGame IO event
            self.log_io_event(event)


class FrameProfiler(BaseLogger):
    _msg = 'Performance over {0} samples: mean={1}ms, std={2}ms'

    def __init__(self, event_manager, name, update=10, window_size=None):
        super(FrameProfiler, self).__init__(event_manager, name)

        from collections import deque
        self.update = update * 1000  # time in s between updates
        self._window_size = window_size
        self.window = deque(maxlen=window_size)  # Samples
        self.t0 = None

    def on_frame(self, event):
        _, total, delta = event
        self.window.append(delta)

        if self.t0 is None:  # skip the first frame
            self.t0 = total
        elif total - self.t0 > self.update:
            if len(self.window) < (self._window_size or float('-inf')):
                self.logger.warn('Window not yet populated.')
            self.log_performance()
            self.t0 = total

    def log_performance(self):
        n = float(len(self.window))
        mean = sum(self.window) / n
        var = sum(map(lambda s: (s - mean) ** 2, self.window))
        std = sqrt(var / (n - 1))

        mean = ('%0.3f' % mean)  # adjust decimal precision
        std = ('%0.3f' % std)
        self.logger.info(self._msg.format(int(n), mean, std))
