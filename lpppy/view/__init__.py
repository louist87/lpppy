#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""lpppy.view:  Submodule for all View classes

A View is responsible for interpreting the data in a Model, and as such,
will never modify data, only represent it and transport it across appropriate
transport layers (display, network, filesystem, etc.)
"""
import lpppy.view.pg
import lpppy.view.logger
