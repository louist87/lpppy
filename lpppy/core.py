#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""lpppy: event-driven psychophysics experiments

Module contents:

lpppy::EventManager
"""
from collections import namedtuple
from weakref import WeakKeyDictionary

import pygame as pg


QUIT_EVENT = pg.event.Event(12)


class Listener(object):
    """Listener mixin"""
    def __init__(self, event_manager, listen=True):
        self.event_manager = event_manager
        if listen:
            self._listening = True
            self.event_manager.register_listener(self)
        else:
            self._listening = False

    @property
    def listening(self):
        return self._listening

    @listening.setter
    def listening(self, value):
        if value != self._listening:
            if value:
                self.event_manager.register_listener(self)
            else:
                self.event_manager.unregister_listener(self)

            self._listening = value

    def notify(self, event):
        # Alias pygame event names
        if isinstance(event.type, int):
            tpe = pg.event.event_name(event.type).lower()
        else:
            tpe = event.type

        # Call callback function
        cb = getattr(self, 'on_{0}'.format(tpe), None)
        if cb is not None:
            cb(event)


class EventManager(object):
    """Coordinate communication between listeners.

    usage:

    >>> with EventManager() as em:
    ....    em.register_listener(SomeListener())
    ....    em.start()
    """
    _StartEvent = namedtuple('StartEvent', ('type'))

    def __init__(self, gamma=1, **kw):
        self.listeners = WeakKeyDictionary()
        self.gamma = gamma
        pg.mixer.pre_init(
            frequency=kw.get('frequency', 44100),
            size=kw.get('size', -16),
            channels=kw.get('channels', 2),
            buffer=kw.get('buffer', 512)
        )

    def __len__(self):
        return len(self.listeners)

    def __enter__(self):
        pg.init()
        pg.display.set_gamma(self.gamma)
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        pg.quit()

    def register_listener(self, listener):
        """Register a listener.

        listener : instance
            Listener instance to register
        """
        self.listeners[listener] = type(listener).__name__

    def unregister_listener(self, listener):
        """Unregister a listener.

        listener : instance
            Listener to unregister
        """
        self.listeners.pop(listener)

    def post(self, event):
        """Notify all listeners of an event.

        event : namedtuple
            Class instance (usually namedtuple)
             exposing a `type` attribute.
        """
        for listener in self.listeners.keys():
            listener.notify(event)

    def start(self):
        self.post(self._StartEvent('start'))
