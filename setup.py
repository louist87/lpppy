#!/usr/bin/env python
# -*- coding: utf-8 -*-
from lpppy import __version__
from setuptools import setup

setup(
    name='lpppy',
    version=__version__,
    author='Louis Thibault',
    author_email='louist87@gmail.com',
    packages=['lpppy'],
    include_package_data=True,
    install_requires=['pygame>=1.9'],
    url='',
    license='GPL 3.0',
    description='Event-driven framework for cognitive science experiments',
    keywords=['LPP',
              'experiment',
              'visual',
              'psychology',
              'cognitive',
              'science',
              'EEG',
              'MEG',
              'MRI',
              'fMRI'],
    long_description='')
