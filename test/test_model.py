#!/usr/bin/env python
# -*- coding: utf-8 -*-
from unittest import TestCase
from types import GeneratorType, MethodType

from lpppy.core import EventManager
from lpppy.model.experiment import Segment


class TestSegment(TestCase):
    class DummySegment(Segment):
        def pre(self, t, dt):
            t, dt = yield "pre {0}{1} 1".format(t, dt)
            t, dt = yield "pre {0}{1} 2".format(t, dt)

        def per(self, t, dt):
            t, dt = yield "per {0}{1} 1".format(t, dt)
            t, dt = yield "per {0}{1} 2".format(t, dt)

        def post(self, t, dt):
            t, dt = yield "post {0}{1} 1".format(t, dt)
            t, dt = yield "post {0}{1} 2".format(t, dt)

    def setUp(self):
        self.segment = self.DummySegment(EventManager())

    def test_init(self):
        self.assertEqual(len(self.segment._coroutines), 3)
        isfunc = map(isinstance, self.segment._coroutines, (MethodType,) * 3)
        self.assertNotIn(False, isfunc)

    def test_call(self):
        callme = self.segment._call_coroutine

        # pre
        self.assertEqual(len(self.segment._coroutines), 3)
        self.assertIsInstance(self.segment._coroutines[0], MethodType)
        self.assertEqual(callme('te', 'st'), "pre test 1")
        self.assertEqual(len(self.segment._coroutines), 3)
        self.assertIsInstance(self.segment._coroutines[0], GeneratorType)
        self.assertEqual(callme('te', 'st'), "pre test 2")
        # per
        self.assertEqual(len(self.segment._coroutines), 3)
        self.assertIsInstance(self.segment._coroutines[0], GeneratorType)
        self.assertEqual(callme('te', 'st'), 'per test 1')
        self.assertEqual(len(self.segment._coroutines), 2)
        self.assertIsInstance(self.segment._coroutines[0], GeneratorType)
        self.assertEqual(callme('te', 'st'), 'per test 2')
        # post
        self.assertEqual(len(self.segment._coroutines), 2)
        self.assertIsInstance(self.segment._coroutines[0], GeneratorType)
        self.assertEqual(callme('te', 'st'), 'post test 1')
        self.assertEqual(len(self.segment._coroutines), 1)
        self.assertIsInstance(self.segment._coroutines[0], GeneratorType)
        self.assertEqual(callme('te', 'st'), 'post test 2')
        # final state
        self.assertEqual(len(self.segment._coroutines), 1)
        self.assertIsInstance(self.segment._coroutines[0], GeneratorType)
        self.assertRaises(StopIteration, callme, 'te', 'st')
        self.assertEqual(len(self.segment._coroutines), 0)
