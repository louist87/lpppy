#!/usr/bin/env python
# -*- coding: utf-8 -*-
from collections import namedtuple

import unittest
import pygame as pg

from lpppy.controller.io import BasicIOController, IOEVS, KEYDOWN, K_c, QUIT
from lpppy.controller.engine import FrameController


def test_ioevs():
    assert not len(IOEVS.symmetric_difference(set(xrange(2, 12))))


class TestFrameController(unittest.TestCase):
    def setUp(self):
        pg.init()
        self.ctrlr = FrameController(self)

    def tearDown(self):
        pg.quit()

    def register_listener(self, listener):
        pass

    def test_initialization(self):
        self.assertIsInstance(self.ctrlr._abstime, int)
        self.assertTrue(self.ctrlr.running)
        self.assertIsInstance(self.ctrlr.frame_rate, int)

    def test_notify(self):
        quitev = namedtuple('TestQuit', ('type'))(12)
        startev = namedtuple('TestStart', ('type'))('start')

        self.assertTrue(self.ctrlr.running)
        self.ctrlr.notify(quitev)
        self.assertFalse(self.ctrlr.running)

        # should simply not throw an error
        self.ctrlr.notify(startev)


class TestBasicIOController(unittest.TestCase):
    def setUp(self):
        self.bioc = BasicIOController(self)

    def register_listener(self, listener):
        pass

    def test_quit_signal(self):
        ev = self.bioc._process_event(pg.event.Event(12))
        self.assertEqual(ev.type, QUIT)

    def test_user_quit(self):
        ev_in = pg.event.Event(KEYDOWN, {'key': 306})
        ev_in2 = pg.event.Event(KEYDOWN, {'key': K_c})
        self.bioc._process_event(ev_in)
        ev = self.bioc._process_event(ev_in2)
        self.assertEqual(ev.type, QUIT)

    def test_nonIO_event(self):
        nonio = namedtuple('TestNonIO', ('type'))('not_io')
        self.assertIs(self.bioc._process_event(nonio), None)
