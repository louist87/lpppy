#!/usr/bin/env python
# -*- coding: utf-8 -*-
from collections import namedtuple

import unittest

from lpppy.core import EventManager, Listener


class TestListener(unittest.TestCase):
    dummy_event = namedtuple('DummyEvent', ('type'))('dummy')

    def setUp(self):
        self.listeners = set()
        self.lstnr = Listener(self)

    def register_listener(self, l):
        self.listeners.add(l)

    def test_autoregister(self):
        self.assertIn(self.lstnr, self.listeners)


class TestEventManager(unittest.TestCase):
    class DummyListener(Listener):
        def notify(self, event):
            global transport
            transport = event

    def setUp(self):
        self.em = EventManager()

    def test_registration(self):
        class Foo(object):
            pass

        listener = Foo()

        self.assertEqual(len(self.em.listeners), 0)
        self.em.register_listener(listener)
        self.assertIn(listener, self.em.listeners)
        self.assertEqual(len(self.em.listeners), 1)
        self.em.unregister_listener(listener)
        self.assertEqual(len(self.em.listeners), 0)
        self.assertNotIn(listener, self.em.listeners)

    def test_post(self):
        test_string = 'Hello, world'

        dl = self.DummyListener(self.em)
        self.em.post(test_string)
        self.assertEqual(transport, test_string)

    def test_start(self):
        dl = self.DummyListener(self.em)
        self.em.start()
        self.assertEqual(transport.type, 'start')
